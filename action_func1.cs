using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        
        //Action // return void
        //Func // returns something ...

        delegate void func_int_returns_void(int x);
        delegate string[] func_int_returns_double(int x, int y, int[] arr);

        //static void Executor(func_int_returns_void f, int x)
        //{
        //    f(x);
        //}

        static void Executor(Action<int> f, int x)
        {
            f(x);
        }


        static double Executor(Func<int, double> f, int x)
        {
            return f(x);
        }


        // use action or function
        // Executor which gets as parameter int, int and invoked the function
        //  From main call this executor with lambda and write an expression which prints their sum 
        // Executor which gets as parameter double and return the result of function invoke
        //  From main call this executor with lambda and write an expression which return their mul
        // Executor which gets as parameter int[] and invoke the function 
        //  From main call this executor with lambda and write an expression which sets all values to zero (void)
        // Executor which gets as parameter double[], double and returns the function invoke 
        //  From main call this executor with lambda and write an expression which returns the sum of all 
        //      elements which are bigger than the second parameter

        static void Main(string[] args)
        {
            
            Executor(x => Console.WriteLine(x * 2), 100);
            Console.WriteLine(Executor(x => Math.Sqrt(x), 91));
        }
    }
}

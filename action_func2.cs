using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        
        //Action // return void
        //Func // returns something ...

        delegate void func_int_returns_void(int x); // ---> Action<int> 
        delegate double func_int_returns_double(int x, int y, int[] arr); // --> Func<int x, int y, int[] arr, string[]>
        delegate int func__no_parameters_return_int();  // ---> Func<int>
        delegate void func_no_args_returns_void(); // Action
        delegate void func_int_arg_returns_void(); // Action<int>
        delegate void func_int_double_float_arg_returns_void(); // Action<int, double, float>
        delegate void func_int_array_arg_returns_void(); // Action<int[]>
        delegate int func_no_args_returns_int(); // Func<int>
        delegate int func_float_arg_returns_string(); // Func<float, string>
        delegate int[] func_string_int_arg_returns_int_rray(); // Func<string, int, int[]>

        //static void Executor(func_int_returns_void f, int x)
        //{
        //    f(x);
        //}

        static void Executor(Action<int> f, int x)
        {
            f(x);
        }


        static double Executor(Func<int, double> f, int a)
        {
            return f(a);
        }


        // use action or function
        // Executor which gets as parameter int, int and invoked the function
        //  From main call this executor with lambda and write an expression which prints their sum 
        // Executor which gets as parameter double and return the result of function invoke
        //  From main call this executor with lambda and write an expression which return their mul
        // Executor which gets as parameter int[] and invoke the function 
        //  From main call this executor with lambda and write an expression which sets all values to zero (void)
        // Executor which gets as parameter double[], double and returns the function invoke 
        //  From main call this executor with lambda and write an expression which returns the sum of all 
        //      elements which are bigger than the second parameter

        static void ExecutorSum(Action<int, int> f, int a, int b)
        {
            f(a, b);
        }

        static double ExecutorMul(Func<double, double, double> f, double a, double b)
        {
            return f(a, b);
        }

        static void ExecutorOnArrayInt(Action<int[]> f)
        {
            int[] arr = { 1, 2, 5, 1001239 };
            f(arr);
        }

        // Executor which gets as parameter double[], double and returns the function invoke 
        //  From main call this executor with lambda and write an expression which returns the sum of all 
        //      elements which are bigger than the second parameter
        static double[] arr = { 1, 2, 5, 1001239, -7.4568, 1231.5345, 2.22222, 3.14 };
        static double min = 6.7;
        static double ExecutorOnArrayDouble(Func<double[], double, double> f)
        {
            double result = f(arr, min);
            return result;
        }

        static void Main(string[] args)
        {
            
            Executor(x => Console.WriteLine(x * 18), 100);
            Console.WriteLine(Executor(x => Math.Sqrt(x), 91));

            ExecutorSum((x, y) => Console.WriteLine($"{x} + {y} = {x+y}"), 10, 20);

            ExecutorMul((x, y) => x * y, 1.555, -98.7);

            ExecutorOnArrayInt(arr =>
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = 0;
                }
            });

            ExecutorOnArrayDouble((rra, nim) =>
            {
                double sum = 0.0;
                foreach (var item in rra)
                {
                    sum = sum + (item > min ? item : 0.0);
                }
                return sum;
            });
        }
    }
}
